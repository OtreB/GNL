/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 12:18:17 by cumberto          #+#    #+#             */
/*   Updated: 2017/01/31 17:14:26 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

t_fd	*ft_choose_fd(int fd, t_fd **list)
{
	t_fd *tmp;

	tmp = *list;
	if (tmp == NULL)
	{
		tmp = ft_memalloc(sizeof(tmp));
		tmp->fd = fd;
		tmp->str = NULL;
		tmp->next = NULL;
		*list = tmp;
		return (tmp);
	}
	while (tmp)
	{
		if (tmp->fd == fd)
			return (tmp);
		tmp = tmp->next;
	}
	tmp = ft_memalloc(sizeof(tmp));
	tmp->fd = fd;
	tmp->str = NULL;
	tmp->next = *list;
	*list = tmp;
	return (tmp);
}

int		ft_take_buff(char **str, int fd)
{
	char	buff[BUFF_SIZE];
	int		ret;
	char	*tmp1;
	char	*tmp2;

	ret = read(fd, buff, BUFF_SIZE);
	if (ret <= 0)
		return (ret);
	if (*str == NULL)
	{
		*str = ft_strnew(ret);
		*str = ft_strncpy(*str, buff, ret);
	}
	else
	{
		tmp1 = *str;
		*str = ft_strnew(ft_strlen(tmp1) + ret);
		*str = ft_strncpy(*str, buff, ret);
		tmp2 = *str;
		*str = ft_strjoin(tmp1, *str);
		free(tmp1);
		free(tmp2);
	}
	return (ret);
}

int		ft_end_of_file(char **str, char ***line)
{
	char	*tmp;
	int		i;

	i = 0;
	while ((*str)[i] != '\0')
		i++;
	if (i == 0)
		return (0);
	tmp = ft_strnew(i);
	ft_strncpy(tmp, *str, i);
	**line = tmp;
	*str = NULL;
	return (1);
}

int		ft_new_line(char **str, char ***line)
{
	char	*tmp1;
	char	*tmp2;
	int		i;
	int		size;

	size = ft_strlen(*str);
	i = 0;
	tmp1 = *str;
	while ((*str)[i] != '\n')
		i++;
	tmp2 = ft_strnew(i);
	ft_strncpy(tmp2, *str, i);
	if (!(*str = ft_strnew(size - i)))
		return (-1);
	ft_strncpy(*str, &tmp1[i + 1], size - i);
	**line = tmp2;
	free(tmp1);
	return (1);
}

int		get_next_line(const int fd, char **line)
{
	static t_fd	*list = NULL;
	t_fd		*tmp;
	int			ret;

	tmp = ft_choose_fd(fd, &list);
	if ((ret = ft_take_buff(&tmp->str, fd)) == -1)
		return (-1);
	if (tmp->str != NULL)
	{
		if (ft_strchr(tmp->str, '\n') != NULL)
			return (ft_new_line(&tmp->str, &line));
		if (ft_strchr(tmp->str, '\0') != NULL && ret != BUFF_SIZE)
			return (ft_end_of_file(&tmp->str, &line));
	}
	else
		return (0);
	return (get_next_line(fd, line));
	return (1);
}
